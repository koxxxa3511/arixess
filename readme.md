##Information
- [Laravel 8 core](https://laravel.com/docs/8.x)

#Default users
- Admin: ['email' => admin@email.com, 'password' => 'admin']
- User: ['email' => user@email.com, 'password' => 'user']
