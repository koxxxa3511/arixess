<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\MailRequestController;

Route::get('/',[UserController::class,'index'])->name('index');
Route::get('users',[UserController::class,'users'])->name('users');
Route::get('user/create',[UserController::class,'create'])->name('user-create');
Route::get('user/{id}/edit',[UserController::class,'edit'])->name('user-edit');
Route::get('user/{id}/delete',[UserController::class,'delete'])->name('user-delete');
Route::post('user/save',[UserController::class,'save'])->name('user-save');

Route::get('settings',[SettingController::class,'index'])->name('settings');
Route::post('settings/save',[SettingController::class,'save'])->name('settings-save');

Route::get('mail-requests',[MailRequestController::class,'index'])->name('mail-requests');
Route::get('mail-request/{mailRequest}/details',[MailRequestController::class,'modal'])->name('mail-request-modal');
Route::get('mail-request/answered/{mailRequest}',[MailRequestController::class,'answered'])->name('mail-request-answered');
