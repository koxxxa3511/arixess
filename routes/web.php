<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MailRequestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['logout' => false]);
Route::get('/logout',[App\Http\Controllers\Auth\LoginController::class,'logout'])->name('logout');

Route::get('/', [App\Http\Controllers\PublicController::class,'index'])->name('index');
Route::middleware('auth')->group(function() {
    Route::view('mail/request', 'public.mail.index')->name('mail-request');
    Route::post('mail/request', [MailRequestController::class, 'save'])->name('mail-request-save');
});

Route::prefix('profile')->group(function(){
    Route::get('/', [App\Http\Controllers\ProfileController::class,'index'])->name('profile');
    Route::post('/save', [App\Http\Controllers\ProfileController::class,'save'])->name('profile-save');
});

//examples
Route::view('/example/modal','example.modal');

