<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailRequestSaveRequest extends FormRequest
{
    public function rules()
    {
        return [
            'subject' => 'required|max:100|string|min:3',
            'content' => 'required',
            'file'    => 'mimes:jpeg,jpg,png|required|max:2000'
        ];
    }

    public function authorize()
    {
        return \Auth::check();
    }

    public function messages()
    {
        return [
            'file.mimes' => 'Тип картинки должен быть одним из следующих типов: *.jpeg, *.jpg, *.png'
        ];
    }
}
