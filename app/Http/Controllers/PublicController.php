<?php

namespace App\Http\Controllers;

use App\Models\Role;

class PublicController extends Controller
{
    public function index()
    {
        $user = \Auth::user();
        if ($user) {
            if ($user->role_id == Role::ID_USER) {
                return redirect()->route('mail-request');
            } else {
                return redirect()->route('admin-mail-requests');
            }
        }
        return view('index');
    }
}
