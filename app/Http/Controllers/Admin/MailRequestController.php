<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MailRequest;
use App\Repositories\MailRequestRepository;

class MailRequestController extends Controller
{
    /**
     * Mail Request repository object
     *
     * @var MailRequestRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $mailRequestRepository;

    /**
     * Controller constructor
     */
    public function __construct()
    {
        $this->mailRequestRepository = app(MailRequestRepository::class);
    }

    /**
     * Page with all Mail Requests with pagination
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $mails = $this->mailRequestRepository->getForAdminTableWithPaginate(20);
        return view('admin.mailRequests.index', compact('mails'));
    }

    /**
     * Get details of Mail Request, showing in ajax modal
     *
     * @param MailRequest $mailRequest
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modal(MailRequest $mailRequest)
    {
        return view('admin.mailRequests.modal', compact('mailRequest'));
    }

    /**
     * Easy function for change is_answered field in Mail Request object
     *
     * @param MailRequest $mailRequest
     * @return bool[]
     */
    public function answered(MailRequest $mailRequest)
    {
        $mailRequest->update([
                                 'is_answered' => !$mailRequest->is_answered
                             ]);
        return [
            'success' => true
        ];
    }
}
