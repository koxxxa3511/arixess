<?php

namespace App\Http\Controllers;

use App\Http\Requests\MailRequestSaveRequest;
use App\Models\MailRequest;

class MailRequestController extends Controller
{
    public function save(MailRequestSaveRequest $request)
    {
        $mailRequest = new MailRequest($request->except('content', '_token'));
        $mailRequest->content = clean($request->input('content'));
        $mailRequest->save();

        // If observer misses saving- save image.
        if ($mailRequest->exists) {
            $mailRequest->update([
                                     'file_path' => $request->file('file')
                                                            ->store('/files/' . now()->toDateString())
                                 ]);
        }
        return back();
    }
}
