<?php

namespace App\Repositories;

use App\Models\MailRequest as Model;

class MailRequestRepository extends CoreRepository
{
    /**
     * Initialize Model
     *
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Get all requests in admin table using paginator
     *
     * @param $perPage
     */

    public function getForAdminTableWithPaginate($perPage)
    {
        $columns = ['id', 'name', 'email', 'subject', 'content', 'file_path', 'is_answered', 'created_at'];
        $result = $this->startConditions()
                       ->select($columns)
                       ->latest('id')
                       ->paginate($perPage);

        return $result;
    }
}
