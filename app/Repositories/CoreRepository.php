<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class CoreRepository{
    /**
     * Repository's model
     *
     * @var Model
     */
    protected $model;

    /**
     * Core Repository constructor
     */
    public function __construct() {
        $this->model = $this->getModelClass();
    }

    /**
     * Function for initialize repository's model
     *
     * @return mixed
     */
    abstract protected function getModelClass();

    /**
     * Function for start work with repository
     *
     * @return Model|mixed
     */
    public function startConditions(){
        return new $this->model;
    }
}
