<?php

namespace App\Jobs;

use App\Mail\SendMail;
use App\Models\MailRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMailRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * Class instance MailRequest
     *
     * @var MailRequest
     */
    private $mailRequest;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MailRequest $mailRequest)
    {
        $this->mailRequest = $mailRequest;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mail = new SendMail($this->mailRequest);
        \Mail::to('admin@email.com')->send($mail);
    }
}
