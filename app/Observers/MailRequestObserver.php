<?php

namespace App\Observers;

use App\Jobs\SendMailRequest;
use App\Models\MailRequest;

class MailRequestObserver
{
    public function creating(MailRequest $mailRequest){
        $user = \Auth::user();
        $hasRecordToday = MailRequest::whereUserId($user->id)->whereDate('created_at', today())->exists();
        if ($hasRecordToday) {
            flash('Only one request per day', 'danger');
            return false;
        }
        $mailRequest->user_id = $user->id;
        $mailRequest->name = $user->name;
        $mailRequest->email = $user->email;
    }
    public function created(MailRequest $mailRequest)
    {
        SendMailRequest::dispatch($mailRequest);
        flash('Request sent');
    }

    public function updated(MailRequest $mailRequest)
    {
        //
    }

    public function deleted(MailRequest $mailRequest)
    {
        //
    }

    public function restored(MailRequest $mailRequest)
    {
        //
    }

    public function forceDeleted(MailRequest $mailRequest)
    {
        //
    }
}
