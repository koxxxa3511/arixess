<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class MailRequest extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'subject', 'content', 'is_answered', 'file_path'
    ];

    protected $appends = [
        'created',
        'image',
        'shortText'
    ];

    public function getCreatedAttribute(){
        return $this->created_at->format('d.m.Y H:i');
    }

    public function getImageAttribute(){
        return '/storage/' . $this->file_path;
    }
    public function getShortTextAttribute(){
        $content = strip_tags($this->content);
        if(Str::length($content) > 20)
            return Str::limit(strip_tags($this->content), 20) . '...';
        else {
            return $content;
        }
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
