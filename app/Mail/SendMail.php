<?php

namespace App\Mail;

use App\Models\MailRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\File;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Class instance for Mail Request
     *
     * @var $mailRequest
     * @mixin MailRequest
     */
    private $mailRequest;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MailRequest $mailRequest)
    {
        $this->mailRequest = $mailRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mailRequest = $this->mailRequest;
        $user = $mailRequest->user;
        return $this->from(ENV('MAIL_FROM_ADDRESS'), ENV('APP_NAME'))
                    ->subject($mailRequest->subject)
                    ->view('mails.mailRequest', compact('mailRequest', 'user'));
    }
}
