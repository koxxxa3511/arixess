<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdToMailRequestsTable extends Migration
{
    public function up()
    {
        Schema::table('mail_requests', function (Blueprint $table) {
            $table->foreignId('user_id')
                  ->after('id')
                  ->constrained('users');
        });
    }

    public function down()
    {
        Schema::table('mail_requests', function (Blueprint $table) {
            $table->dropForeign('user_id');
            $table->dropColumn('user_id');
        });
    }
}
