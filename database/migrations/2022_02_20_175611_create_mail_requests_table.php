<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailRequestsTable extends Migration
{
    public function up()
    {
        Schema::create('mail_requests', function (Blueprint $table) {
            $table->id();

            $table->string('name', 100);
            $table->string('email', 100);
            $table->string('subject', 100);
            $table->text('content');
            $table->string('file_path')->nullable(); // Can make a filepath in separate table, but now there is no such task.
            $table->boolean('is_answered')->default(false);

            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mail_requests');
    }
}
