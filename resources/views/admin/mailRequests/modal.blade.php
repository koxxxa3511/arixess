@php
    /**
     * @var $mailRequest
     * @mixin \App\Models\MailRequest
     */
@endphp

<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Mail #{{ $mailRequest->id }}: Content detail
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-4">
                        <img src="{{ $mailRequest->image }}" alt="" class="img-fluid">
                    </div>
                    <div class="col-lg">
                        {!! $mailRequest->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
