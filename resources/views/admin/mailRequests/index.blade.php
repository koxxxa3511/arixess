@extends('layouts.admin')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/admin">@lang('Admin Panel')</a></li>
            <li class="breadcrumb-item active" aria-current="page">@lang('Mail requests')</li>
        </ol>
    </nav>
    <div class="table-responsive" id="summery">
        <table class="table table-hover table-striped table-sm">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>E-Mail</th>
                <th>Subject</th>
                <th>Content</th>
                <th>File</th>
                <th>Is answered</th>
                <th>Created date</th>
            </tr>
            </thead>
            <tbody>
            @foreach($mails as $mail)
                <tr>
                    <td>{{ $mail->id }}</td>
                    <td>{{ $mail->name }}</td>
                    <td>{{ $mail->email }}</td>
                    <td>{{ $mail->subject }}</td>
                    <td>
                        <a href="{{ route('admin-mail-request-modal', $mail) }}" class="btn btn-link ajax-modal">
                        {!! $mail->shortText !!} <!-- Mutator in MailRequest model -->
                        </a>
                    </td>
                    <td>
                        <a href="{{ $mail->image }}" data-fancybox> <!-- Mutator in MailRequest model -->
                            <img src="{{ $mail->image }}" alt="" class="img-fluid" width="50">
                        </a>
                    </td>
                    <td>
                        <label for="is_answered">
                            <input type="checkbox" name="is_answered" id="is_answered"
                                   value="{{ $mail->id }}" {{ attr_checked($mail->is_answered) }}>
                            Answered
                        </label>
                    </td>
                    <td>{{ $mail->created }}</td> <!-- Mutator in MailRequest model -->
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="d-flex justify-content-center">
        {{ $mails->links() }}
    </div>
@endsection
@push('head')
    <style>
        .table tr td,
        .table tr td,
        .table tr td a{
            font-size: 13px;
        }
        .table tr td{
            vertical-align: middle;
        }
    </style>
@endpush
@push('scripts')
    <script>
        $(function (){
            $(document).on('click', 'input:checkbox', function(){
                $.ajax({
                    url: '/admin/mail-request/answered/' + $(this).val(),
                    method: 'GET',
                    success: function(response){
                        if(response.success) {
                            flash('Success', 'success');
                        } else {
                            alert('Error');
                        }
                    },
                    fail: function(){
                        alert('Error!');
                    }
                })
            });
        });
    </script>
@endpush
