@php
    /**
     * @var $mailRequest
     * @mixin \App\Models\MailRequest
     */

@endphp

<!DOCTYPE html>
<html>
<head>
    <title>{{ $mailRequest->subject }}</title>
    <style>
        p{
            margin-bottom: 10px;
        }
        .font-weight-bold{
            font-weight: 700;
        }
    </style>
</head>
<body>

<center>
    <h2 style="padding: 23px;background: #b3deb8a1;border-bottom: 6px green solid;">
        <a href="https://itsolutionstuff.com">Visit Our Website : ItSolutionStuff.com</a>
    </h2>
</center>

<p>Hi, Sir</p>
<p>You have a new mail request</p>
<p><strong>Name:</strong> {{ $user->name }}</p>
<p><strong>E-Mail:</strong> {{ $user->email }}</p>
<p><strong>Request Mail ID:</strong> {{ $mailRequest->id }}</p>
<p><strong>Created:</strong>  {{ $mailRequest->created }}</p>
<p class="font-weight-bold">Content: </p>
{!! $mailRequest->content !!}

<img src="{{ $message->embed(storage_path('app/public/' . $mailRequest->file_path)) }}" alt="">

</body>
</html>
