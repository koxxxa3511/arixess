@extends('layouts.public')

@section('content')
    <div class="row justify-content-center align-items-center" id="page">
        <div class="col-lg-6">
            <div class="shadow bg-white p-3 rounded">
                <h4 class="text-center">Callback form</h4>
                <form action="{{ route('mail-request-save') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-2">
                        <label for="subject">Subject</label>
                        <input type="text" name="subject" id="subject"
                               class="form-control @error('subject') is-invalid @enderror"
                               required value="{{ old('subject') }}">

                        @error('subject')
                        <alert class="alert-danger mt-2">
                            {{ $message }}
                        </alert>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="content">Content</label>
                        <textarea name="content" id="content" rows="8"
                                  class="form-control @error('content') is-invalid @enderror"
                                  required>{{ old('content') }}</textarea>

                        @error('content')
                        <alert class="alert-danger mt-2">
                            {{ $message }}
                        </alert>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="file">Accompanying file</label>
                        <!--I don't know what type of file needing, so i using Image-->
                        <br>
                        <input type="file" name="file" id="file" required accept="image/jpeg, image/png">

                        @error('file')
                        <div class="alert alert-danger mt-2 role=alert">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary btn-sm">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('head')
    <style>
        #page {
            min-height: calc(100vh - 54px); /*Subtract navbar's height*/
        }

        #page .input-group .form-control {
            border-top-left-radius: 8px;
            border-bottom-left-radius: 8px;
        }

        #page .input-group .input-group-append {
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #ccc;
            min-height: calc(1.6em + .75rem + 2px);
            border-top-right-radius: 8px;
            border-bottom-right-radius: 8px;
            color: #ffffff;
        }
    </style>
@endpush
