@extends('layouts.public')

@section('content')
    <div class="row justify-content-center align-items-center" id="page">
        <div class="col-lg-4">
            <div class="shadow bg-white p-3 rounded">
                <h4 class="text-center">Login</h4>
                <form action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="mb-2">
                        <label for="email">Email</label>
                        <div class="input-group">
                            <input type="email" name="email" id="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   value="{{ old('email') }}" required>
                            <div class="input-group-append px-2">
                                <i class="fa fa-envelope"></i>
                            </div>
                        </div>

                        @error('email')
                        <div class="alert alert-danger mt-2" role="alert">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-2">
                        <label for="password">Password</label>
                        <div class="input-group">
                            <input type="password" name="password" id="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                   required>
                            <div class="input-group-append px-2">
                                <i class="fa fa-key"></i>
                            </div>
                        </div>

                        @error('password')
                            <div class="alert alert-danger mt-2" role="alert">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary btn-sm">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('head')
    <style>
        #page {
            min-height: calc(100vh - 54px); /*Subtract navbar's height*/
        }

        #page .input-group .form-control {
            border-top-left-radius: 8px;
            border-bottom-left-radius: 8px;
        }

        #page .input-group .input-group-append {
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #ccc;
            min-height: calc(1.6em + .75rem + 2px);
            border-top-right-radius: 8px;
            border-bottom-right-radius: 8px;
            color: #ffffff;
        }
    </style>
@endpush
